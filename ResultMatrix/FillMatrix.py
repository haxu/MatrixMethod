from ROOT import *
import sys

def main(argv):
    #print argv
    RATIO = "kkk"
    SCALE = "ppp"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    SP = argv[0]
    WP = argv[1] 
    
    gStyle.SetOptStat(False)
    inputfile  = TFile("/sps/atlas/h/hanlin/QMISID/batchOutput/"+SP+"_"+WP+".root",'READ')
    outputfile = TFile(SP+"_"+WP+"_MatrixOutput.root",'recreate')  
   # WP = 'TightT'

    outputfile.cd()
    M2DW=TH2F("Matrix2DWwong","Matrix2DWrong",9,0,9,9,0,9)
    M2DR=TH2F("Matrix2DRight","Matrix2DRight",9,0,9,9,0,9)
    M2DRR=TH2F("Matrix2DRightReverse","Matrix2DRightReverse",9,0,9,9,0,9)
    E2DW=TH2F("Entry2DWrong","Entry2DWrong",6,0,6,4,0,4)
    E2DR=TH2F("Entry2DRight","Entry2DRight",6,0,6,4,0,4)
    print "Reco to Truth Wrong Weighted"
    Can1 = TCanvas("c1","c1",1000,700)
    Can1.SetRightMargin(2)
    etalist = ['0.00', '0.60', '1.10', '1.52', '1.70', '2.30']
    for eta2 in etalist:   
      for et1 in ['0','4','1','2','3','5','6','7','8']:
        h1 = inputfile.Get(WP+"_Matrix_Reco/Matrix_et"+et1+"eta"+eta2).Clone()
        h2 = inputfile.Get(WP+"_MatrixUW_Reco/MatrixUW_et"+et1+"eta"+eta2).Clone()
        if h1.Integral()>0:
           h1.Scale(1./h1.Integral()) 
        #E2DW.SetBinContent(etalist.index(eta2)+1,int(et1),h2.Integral())
        #print h1.GetBinContent(1),h1.GetBinContent(2),h1.GetBinContent(3),h1.GetBinContent(4)
        M2DW.SetBinContent(int(et1)+1,1,h1.GetBinContent(1))
        M2DW.SetBinContent(int(et1)+1,2,h1.GetBinContent(2))
        M2DW.SetBinContent(int(et1)+1,3,h1.GetBinContent(3))
        M2DW.SetBinContent(int(et1)+1,4,h1.GetBinContent(4))
        M2DW.SetBinContent(int(et1)+1,5,h1.GetBinContent(5))
        M2DW.SetBinContent(int(et1)+1,6,h1.GetBinContent(6))
        M2DW.SetBinContent(int(et1)+1,7,h1.GetBinContent(7))
        M2DW.SetBinContent(int(et1)+1,8,h1.GetBinContent(8))
        M2DW.SetBinContent(int(et1)+1,9,h1.GetBinContent(9))
      M2DW.GetXaxis().SetTitle("P_{T}^{Reco} (GeV)")
      
      M2DW.GetYaxis().SetTitle("P_{T}^{Truth} (GeV)")
      M2DW.GetXaxis().SetBinLabel(1,"below 25")
      M2DW.GetXaxis().SetBinLabel(2,"25-30")     
      M2DW.GetXaxis().SetBinLabel( 3,"30-40")
      M2DW.GetXaxis().SetBinLabel( 4,"40-50")
      M2DW.GetXaxis().SetBinLabel( 5,"50-60")
      M2DW.GetXaxis().SetBinLabel( 6,"60-70")
      M2DW.GetXaxis().SetBinLabel( 7,"70-95")
      M2DW.GetXaxis().SetBinLabel( 8,"95-130")
      M2DW.GetXaxis().SetBinLabel( 9,"130-1000")

      M2DW.GetYaxis().SetBinLabel(1,"below 25")
      M2DW.GetYaxis().SetBinLabel(2,"25-30")
      M2DW.GetYaxis().SetBinLabel( 3,"30-40")
      M2DW.GetYaxis().SetBinLabel( 4,"40-50")
      M2DW.GetYaxis().SetBinLabel( 5,"50-60")
      M2DW.GetYaxis().SetBinLabel( 6,"60-70")
      M2DW.GetYaxis().SetBinLabel( 7,"70-95")
      M2DW.GetYaxis().SetBinLabel( 8,"95-130")
      M2DW.GetYaxis().SetBinLabel( 9,"130-1000")
      M2DW.SetTitle("Matrix2DWrong_"+eta2)
      M2DW.SetName("Matrix2DWrong_"+eta2)    
      M2DW.Write()
      M2DW.Draw("colz")
      M2DW.Draw("textsame")
      #Can1.Print("MatrixWrong"+eta2+".pdf")
    print "Reco to Truth Right Unweighted" 
    for eta2 in etalist:   
      for et1 in ['0','1','2','3','4','5','6','7','8']:
        h2 = inputfile.Get(WP+"_MatrixUWR_Reco/MatrixUWR_et"+et1+"eta"+eta2).Clone()
        h1 = inputfile.Get(WP+"_MatrixR_Reco/MatrixR_et"+et1+"eta"+eta2).Clone()
        h3 = inputfile.Get(WP+"_RMatrixR_Reco/RMatrixR_et"+et1+"eta"+eta2).Clone()
        #E2DR.SetBinContent(etalist.index(eta2)+1,int(et1)+1,h2.Integral())
        #print h1.GetEntries()
        if h1.Integral()>0:
           h1.Scale(1./h1.Integral()) 
        if h3.Integral()>0:
           h3.Scale(1./h3.Integral())
        #eta =etalist.index(eta2)
       # print h1.GetBinContent(1),h1.GetBinContent(2),h1.GetBinContent(3),h1.GetBinContent(4)
        M2DR.SetBinContent(int(et1)+1,1,h1.GetBinContent(1))
        M2DR.SetBinContent(int(et1)+1,2,h1.GetBinContent(2))
        M2DR.SetBinContent(int(et1)+1,3,h1.GetBinContent(3))
        M2DR.SetBinContent(int(et1)+1,4,h1.GetBinContent(4))
        M2DR.SetBinContent(int(et1)+1,5,h1.GetBinContent(5))
        M2DR.SetBinContent(int(et1)+1,6,h1.GetBinContent(6))
        M2DR.SetBinContent(int(et1)+1,7,h1.GetBinContent(7))
        M2DR.SetBinContent(int(et1)+1,8,h1.GetBinContent(8))
        M2DRR.SetBinContent(int(et1)+1,1,h3.GetBinContent(1))
        M2DRR.SetBinContent(int(et1)+1,2,h3.GetBinContent(2))
        M2DRR.SetBinContent(int(et1)+1,3,h3.GetBinContent(3))
        M2DRR.SetBinContent(int(et1)+1,4,h3.GetBinContent(4))
        M2DRR.SetBinContent(int(et1)+1,5,h3.GetBinContent(5))
        M2DRR.SetBinContent(int(et1)+1,6,h3.GetBinContent(6))
        M2DRR.SetBinContent(int(et1)+1,7,h3.GetBinContent(7))
        M2DRR.SetBinContent(int(et1)+1,8,h3.GetBinContent(8))
        M2DR.SetBinContent(int(et1)+1,9,h1.GetBinContent(9))
        M2DRR.SetBinContent(int(et1)+1,9,h3.GetBinContent(9))
      M2DR.GetXaxis().SetTitle("P_{T}^{Reco} (GeV)")
      M2DR.GetXaxis().SetBinLabel(1,"below 25")
      M2DR.GetXaxis().SetBinLabel(2,"25-30")
      M2DR.GetXaxis().SetBinLabel( 3,"30-40")
      M2DR.GetXaxis().SetBinLabel( 4,"40-50")
      M2DR.GetXaxis().SetBinLabel( 5,"50-60")
      M2DR.GetXaxis().SetBinLabel( 6,"60-70")
      M2DR.GetXaxis().SetBinLabel( 7,"70-95")
      M2DR.GetXaxis().SetBinLabel( 8,"95-130")
      M2DR.GetXaxis().SetBinLabel( 9,"130-1000")

      M2DR.GetYaxis().SetBinLabel(1,"below 25")
      M2DR.GetYaxis().SetBinLabel(2,"25-30")
      M2DR.GetYaxis().SetBinLabel( 3,"30-40")
      M2DR.GetYaxis().SetBinLabel( 4,"40-50")
      M2DR.GetYaxis().SetBinLabel( 5,"50-60")
      M2DR.GetYaxis().SetBinLabel( 6,"60-70")
      M2DR.GetYaxis().SetBinLabel( 7,"70-95")
      M2DR.GetYaxis().SetBinLabel( 8,"95-130")
      M2DR.GetYaxis().SetBinLabel( 9,"130-1000")
      M2DRR.GetXaxis().SetBinLabel(1,"below 25")
      M2DRR.GetXaxis().SetBinLabel(2,"25-30")
      M2DRR.GetXaxis().SetBinLabel( 3,"30-40")
      M2DRR.GetXaxis().SetBinLabel( 4,"40-50")
      M2DRR.GetXaxis().SetBinLabel( 5,"50-60")
      M2DRR.GetXaxis().SetBinLabel( 6,"60-70")
      M2DRR.GetXaxis().SetBinLabel( 7,"70-95")
      M2DRR.GetXaxis().SetBinLabel( 8,"95-130")
      M2DRR.GetXaxis().SetBinLabel( 9,"130-1000")

      M2DRR.GetYaxis().SetBinLabel(1,"below 25")
      M2DRR.GetYaxis().SetBinLabel(2,"25-30")
      M2DRR.GetYaxis().SetBinLabel( 3,"30-40")
      M2DRR.GetYaxis().SetBinLabel( 4,"40-50")
      M2DRR.GetYaxis().SetBinLabel( 5,"50-60")
      M2DRR.GetYaxis().SetBinLabel( 6,"60-70")
      M2DRR.GetYaxis().SetBinLabel( 7,"70-95")
      M2DRR.GetYaxis().SetBinLabel( 8,"95-130")
      M2DRR.GetYaxis().SetBinLabel( 9,"130-1000")



      M2DR.GetYaxis().SetTitle("P_{T}^{Truth} (GeV)")




      M2DRR.GetYaxis().SetTitle("P_{T}^{Reco} (GeV)")
















 
      M2DRR.GetXaxis().SetTitle("P_{T}^{Truth} (GeV)")
     
    
   
  
      M2DR.SetTitle("Matrix2DRight_"+eta2)
      M2DR.SetName("Matrix2DRight_"+eta2) 
      M2DR.Write()
      M2DR.Draw("colz")
      M2DR.Draw("textsame")
      #Can1.Print("MatrixRight"+eta2+".pdf")
      M2DRR.SetTitle("Matrix2DRightReverse_"+eta2)
      M2DRR.SetName("Matrix2DRightReverse_"+eta2) 
      M2DRR.Write()
      M2DRR.Draw("colz")
      M2DRR.Draw("textsame")
      #Can1.Print("MatrixRightReverse"+eta2+".pdf")

    #outputfile.cd()
#    M2DW.Write()
#    M2DR.Write()
#    M2DRR.Write()
    E2DW.Write()
    E2DR.Write() 
   


 
 
 
 
 
 
 
 
 
 
 
 
 
 

      #Can1.Print("MatrixRight"+eta2+".pdf")

  
  
  
  
  

  
  
  
  
  
  
  
  
  
  
  

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
         
    return
main(sys.argv[1:])

