from ROOT import *

def main():
    RATIO = "kkk"
    SCALE = "ppp"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetOptStat(False)
    inputfile  = TFile("/sps/atlas/h/hanlin/QMISID/Matrix.root",'READ')
    
    


    M2DW=TH2F("M2DW","M2DW",4,0,4,4,0,4)
    M2DR=TH2F("M2DR","M2DR",4,0,4,4,0,4)
    print "Reco to Truth Wrong Weighted"
    Can1 = TCanvas("c1","c1",1000,700)
    Can1.SetRightMargin(2)
    etalist = ['0.00', '0.60', '1.10', '1.52', '1.70', '2.30']
    for eta2 in etalist:   
      for et1 in ['0','1','2','3']:
        h1 = inputfile.Get("Tight_Matrix_Reco/Matrix_et"+et1+"eta"+eta2).Clone()
        #print h1.GetEntries(),"   ",et1,"   ",eta2
        if h1.Integral()>0:
           h1.Scale(1./h1.Integral()) 
        
        #print h1.GetBinContent(1),h1.GetBinContent(2),h1.GetBinContent(3),h1.GetBinContent(4)
        M2DW.SetBinContent(int(et1)+1,1,h1.GetBinContent(1))
        M2DW.SetBinContent(int(et1)+1,2,h1.GetBinContent(2))
        M2DW.SetBinContent(int(et1)+1,3,h1.GetBinContent(3))
        M2DW.SetBinContent(int(et1)+1,4,h1.GetBinContent(4))
      M2DW.Draw("colz")
      M2DW.Draw("textsame")
      M2DW.GetXaxis().SetTitle("P_{T}^{Reco}")
      M2DW.GetYaxis().SetTitle("P_{T}^{Truth}")
      Can1.Print("MatrixWrong"+eta2+".pdf")
    print "Reco to Truth Right Unweighted" 
    for eta2 in etalist:   
      for et1 in ['0','1','2','3']:
        
        h1 = inputfile.Get("Tight_MatrixR_Reco/MatrixR_et"+et1+"eta"+eta2).Clone()
        #print h1.GetEntries()
        if h1.Integral()>0:
           h1.Scale(1./h1.Integral()) 
        #eta =etalist.index(eta2)
       # print h1.GetBinContent(1),h1.GetBinContent(2),h1.GetBinContent(3),h1.GetBinContent(4)
        M2DW.SetBinContent(int(et1)+1,1,h1.GetBinContent(1))
        M2DW.SetBinContent(int(et1)+1,2,h1.GetBinContent(2))
        M2DW.SetBinContent(int(et1)+1,3,h1.GetBinContent(3))
        M2DW.SetBinContent(int(et1)+1,4,h1.GetBinContent(4))
      M2DW.Draw("colz")
      M2DW.Draw("textsame")
      M2DW.GetXaxis().SetTitle("P_{T}^{Reco}")
      M2DW.GetYaxis().SetTitle("P_{T}^{Truth}")

      Can1.Print("MatrixRight"+eta2+".pdf")

  
  
  
  
  

  
  
  
  
  
  
  
  
  
  
  

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

  
  
  
  
  
  
  
  
  
  










        
    return
main()

