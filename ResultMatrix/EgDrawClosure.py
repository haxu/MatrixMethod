from ROOT import *
import math
from numpy import *
def main():
    RATIO = "kkk"
    SCALE = "ppp"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetOptStat(False)
    gStyle.SetLineWidth(1)
    gStyle.SetMarkerSize(0.5)
    inputfile  = TFile("/sps/atlas/h/hanlin/QMISID/Matrix.root",'READ')
    MatrixFile = TFile("./MatrixOutput.root",'READ')
    hR = TH1F("Ratio","Ratio",24,0,24) 
    h_Rate_Origin = TH1F("Origin","Origin",24,0,24)
    h_Rate_Truth = TH1F("Truth","Truth",24,0,24)
    h_Rate_Origin_WM = TH1F("Origin_WM","Origin_WM",24,0,24)
    h_Rate_Origin_WMRM = TH1F("Origin_WMRM","Origin_WMRM",24,0,24)
    h_Rate_Origin_WMRRM = TH1F("Origin_WMRRM","Origin_WMRRM",24,0,24)
    
    ListCountingW= [ [ 0 for i in range(5) ] for j in range(6) ] 
    ListCountingR= [ [ 0 for i in range(5) ] for j in range(6) ]   
    ListCountingWbkp= [ [ 0 for i in range(5) ] for j in range(6) ]
    ListCountingRbkp= [ [ 0 for i in range(5) ] for j in range(6) ]
    #list WM eta bin
    ListWM =  [  [[ 0 for i in range(5) ] for j in range(5)] for k in range(6) ]
    ListRM =  [  [[ 0 for i in range(5) ] for j in range(5)] for k in range(6) ]
    ListRRM =  [  [[ 0 for i in range(5) ] for j in range(5)] for k in range(6) ]
    #print ListWM
    etalist = ['0.00', '0.60', '1.10', '1.52', '1.70', '2.30']  
    ETBIN = [25.0, 60.0, 90.0, 130.0, 1000.0]
    ETABIN = [0.00, 0.60, 1.10, 1.52, 1.70, 2.30, 2.50]
    hCountingRecoW = inputfile.Get("Counting_W").Clone()
    hCountingRecoR = inputfile.Get("Counting_R").Clone()
    hCountingRecoWT = inputfile.Get("Counting_WT").Clone()
    hCountingRecoRT = inputfile.Get("Counting_RT").Clone()
    hCbkpW = MatrixFile.Get("Entry2DWrong").Clone()
    hCbkpR = MatrixFile.Get("Entry2DRight").Clone()
    hCountingRecoR.Add(hCountingRecoW)
    hCountingRecoRT.Add(hCountingRecoWT)
    hCountingRecoR2=inputfile.Get("Counting_R").Clone()
    hCountingRecoRT2=inputfile.Get("Counting_RT").Clone()

    #hCountingRecoW.Divide(hCountingRecoR)
    #hCountingRecoWT.Divide(hCountingRecoRT)
    PerformW = inputfile.Get("Counting_W").Clone()
    PerformWT = inputfile.Get("Counting_WT").Clone()
    PerformW.Divide(hCountingRecoR)
    PerformWT.Divide(hCountingRecoRT)
    for Ceta2 in range(0,6):
      htempW = MatrixFile.Get("Matrix2DWrong_"+str(etalist[Ceta2])).Clone()
      htempR = MatrixFile.Get("Matrix2DRight_"+str(etalist[Ceta2])).Clone()
      htempRR = MatrixFile.Get("Matrix2DRightReverse_"+str(etalist[Ceta2])).Clone()
      for Cet1 in range(0,5):
     #OriginFill
        
        ListCountingW[Ceta2][Cet1]=hCountingRecoW.GetBinContent(Cet1+1,Ceta2+1)
        ListCountingR[Ceta2][Cet1]=hCountingRecoR.GetBinContent(Cet1+1,Ceta2+1)
        ListCountingWbkp[Ceta2][Cet1] =  hCountingRecoW.GetBinContent(Cet1+1,Ceta2+1)
        ListCountingRbkp[Ceta2][Cet1] =  hCountingRecoR2.GetBinContent(Cet1+1,Ceta2+1)
        for Cet2 in range(0,5):
          
          #MatrixFile.Get("Matrix2DWrong_"+str(etalist[Ceta2])).Clone() 
          ListWM[Ceta2][Cet1][Cet2]=htempW.GetBinContent(1+Cet2,1+Cet1)
          ListRM[Ceta2][Cet1][Cet2]=htempR.GetBinContent(1+Cet2,1+Cet1)
          ListRRM[Ceta2][Cet1][Cet2]=htempRR.GetBinContent(1+Cet2,1+Cet1)
        if Cet1 in [4]:
           continue
        print Cet1 
        h_Rate_Origin.SetBinContent(Cet1+Ceta2*4+1,hCountingRecoW.GetBinContent(Cet1+2,Ceta2+1)/(hCountingRecoR.GetBinContent(Cet1+2,Ceta2+1))) 
        ERRA = hCountingRecoW.GetBinError(Cet1+2,Ceta2+1)/hCountingRecoW.GetBinContent(Cet1+2,Ceta2+1)
        ERRB = hCountingRecoR.GetBinError(Cet1+2,Ceta2+1)/hCountingRecoR.GetBinContent(Cet1+2,Ceta2+1) 
        ERROR = math.sqrt(ERRA*ERRA+ERRB*ERRB)
        h_Rate_Origin.SetBinError(Cet1+Ceta2*4+1,PerformW.GetBinError(Cet1+2,Ceta2+1))
     #TruthFill
        ERRA = hCountingRecoW.GetBinError(Cet1+2,Ceta2+1)/hCountingRecoWT.GetBinContent(Cet1+2,Ceta2+1)
        ERRB = hCountingRecoR.GetBinError(Cet1+2,Ceta2+1)/hCountingRecoRT.GetBinContent(Cet1+2,Ceta2+1)
        ERROR = math.sqrt(ERRA*ERRA+ERRB*ERRB)
        h_Rate_Truth.SetBinContent(Cet1+Ceta2*4+1,hCountingRecoWT.GetBinContent(Cet1+2,Ceta2+1)/hCountingRecoRT.GetBinContent(Cet1+2,Ceta2+1))
        h_Rate_Truth.SetBinError(Cet1+Ceta2*4+1,PerformWT.GetBinError(Cet1+2,Ceta2+1))
     #Perform Matrix Filling
    CTW = []
    CTR = []
    CTWbkp = []
    CTRbkp = []
    CTTW = []
    CTTR = []
    ListMatrixW = []
    ListMatrixR = []
    ListMatrixRR = []
    for Ceta2 in range(0,6):
       CTW.append(mat(ListCountingW[Ceta2]).T)
       CTR.append(mat(ListCountingR[Ceta2]).T)
       CTWbkp.append(mat(ListCountingWbkp[Ceta2]).T)
       CTRbkp.append(mat(ListCountingRbkp[Ceta2]).T)
       ListMatrixW.append(mat(ListWM[Ceta2]))
       ListMatrixR.append(mat(ListRM[Ceta2]))
       ListMatrixRR.append((mat(ListRRM[Ceta2]))*mat(ListWM[Ceta2]))
    print CTWbkp
    #Matrix Shift Filling Wrong Matrix Only
    Output1W = []
    Output1R = CTRbkp
    for Ceta2 in range(0,6):
       Output1W.append(ListMatrixW[Ceta2]*CTWbkp[Ceta2])
    #Matrix Shift Filling Wrong Matrix Right Matrix
    Output2W = Output1W 
    Output2R = []
    for Ceta2 in range(0,6):
       Output2R.append(ListMatrixR[Ceta2]*CTRbkp[Ceta2])
    #Matrix Shift Filling Wrong Matrix Reverse Right Matrix
    Output3W = []
    Output3R = []
    for Ceta2 in range(0,6):
       Output3W.append(ListMatrixRR[Ceta2]*CTWbkp[Ceta2])
       Output3R.append(mat(ListRRM[Ceta2])*mat(ListRM[Ceta2])*CTRbkp[Ceta2])
    #Hist Filling
    for Ceta2 in range(0,6):
       for Cet1 in range(1,5):
         h_Rate_Origin_WM.SetBinContent(Cet1+Ceta2*4,Output1W[Ceta2][Cet1]/(Output1R[Ceta2][Cet1]+Output1W[Ceta2][Cet1]))     
         ERRA = math.sqrt(1./Output1W[Ceta2][Cet1])
         ERRB = math.sqrt(1./(Output1R[Ceta2][Cet1]+Output1W[Ceta2][Cet1]))
         ERROR = math.sqrt(ERRA*ERRA+ERRB*ERRB)
         h_Rate_Origin_WM.SetBinError(Cet1+Ceta2*4,ERROR*(Output1W[Ceta2][Cet1]/(Output1R[Ceta2][Cet1]+Output1W[Ceta2][Cet1])))

         h_Rate_Origin_WMRM.SetBinContent(Cet1+Ceta2*4,Output2W[Ceta2][Cet1]/(Output2R[Ceta2][Cet1]+Output2W[Ceta2][Cet1]))
         ERRA = math.sqrt(1./Output2W[Ceta2][Cet1])
         ERRB = math.sqrt(1./(Output2R[Ceta2][Cet1]+Output2W[Ceta2][Cet1]))
         ERROR = math.sqrt(ERRA*ERRA+ERRB*ERRB)
         h_Rate_Origin_WMRM.SetBinError(Cet1+Ceta2*4,ERROR*(Output2W[Ceta2][Cet1]/(Output2R[Ceta2][Cet1]+Output2W[Ceta2][Cet1])))

         h_Rate_Origin_WMRRM.SetBinContent(Cet1+Ceta2*4,Output3W[Ceta2][Cet1]/(Output3R[Ceta2][Cet1]+Output3W[Ceta2][Cet1]))
         ERRA = math.sqrt(1./Output3W[Ceta2][Cet1])
         ERRB = math.sqrt(1./(Output3R[Ceta2][Cet1]+Output3W[Ceta2][Cet1]))
         ERROR = math.sqrt(ERRA*ERRA+ERRB*ERRB)
         h_Rate_Origin_WMRRM.SetBinError(Cet1+Ceta2*4,ERROR*(Output3W[Ceta2][Cet1]/(Output3W[Ceta2][Cet1]+Output3R[Ceta2][Cet1])))
    
    Rates = [0.00036324
             ,0.000530928
             ,0.001145205
             ,0.002834449
             ,0.00087175
             ,0.001532757
             ,0.002548245
             ,0.005595551
             ,0.002253963
             ,0.004478557
             ,0.006131852
             ,0.012600712
             ,0.007031106
             ,0.012652089
             ,0.020856902
             ,0.032518099
             ,0.009976358
             ,0.020120309
             ,0.03561939
             ,0.053641001
             ,0.025286743
             ,0.049390867
             ,0.070881275
             ,0.098250364
             ]




    Err = [9.18e-06
             ,4.55e-05
             ,0.000168065
             ,0.000373041
             ,1.53e-05
             ,8.05e-05
             ,0.000277263
             ,0.000625652
             ,3.25e-05
             ,0.000189314
             ,0.00060822
             ,0.001401545
             ,7.21e-05
             ,0.000414246
             ,0.00147632
             ,0.003165863
             ,4.85e-05
             ,0.000333615
             ,0.001210676
             ,0.00256157
             ,0.000168706
             ,0.001155658
             ,0.003977445
             ,0.00889067]
    for i in range(0,24):
        hR.SetBinContent(i+1,Rates[i])
        hR.SetBinError(i+1,Err[i])

    Can1 = TCanvas("c1","c1",1000,1000)
    #Can1.SetLogy()
    pad1 = TPad("pad1","",0.01,0.35,0.95,0.95);
    pad1.Draw();
    pad1.SetLogy()
    pad2 = TPad("pad2","",0.01,0.05,0.95,0.35);
    pad2.Draw();
    pad2.SetTopMargin(0)
    pad1.SetBottomMargin(0)
    pad1.cd()
    h_Rate_Origin.Draw("e")
    line1 = TLine(4,0,4,0.2)
    line1.Draw("same")
    line2 = TLine(8,0,8,0.2)
    line2.Draw("same")
    line3 = TLine(12,0,12,0.2)
    line3.Draw("same")
    line4 = TLine(16,0,16,0.2)
    line4.Draw("same")
    line6 = TLine(20,0,20,0.2)
    line6.Draw("same")
    h_Rate_Truth.SetMarkerColor(kRed)
    h_Rate_Truth.SetLineColor(kRed)
    h_Rate_Truth.Draw("esame")
    h_Rate_Origin_WM.SetMarkerColor(kBlue)
    h_Rate_Origin_WM.SetLineColor(kBlue)
    #h_Rate_Origin_WM.Draw("esame")
    h_Rate_Origin_WMRM.SetMarkerColor(kGreen)
    h_Rate_Origin_WMRM.SetLineColor(kGreen)
    h_Rate_Origin_WMRM.Draw("esame")
    h_Rate_Origin.GetYaxis().SetTitle("Charge Flip Rate")
    h_Rate_Origin_WMRRM.SetMarkerColor(kViolet)
    h_Rate_Origin_WMRRM.SetLineColor(kViolet)
    hR.SetLineColor(kYellow)
    hR.SetMarkerColor(kYellow)
    #hR.Draw("esame")
    #h_Rate_Origin_WMRRM.Draw("esame")
    leg = TLegend(0.2,0.65,0.4,0.85)
    leg.AddEntry(h_Rate_Origin,"Rates with Reco P_{T}","apl")
    leg.AddEntry(h_Rate_Truth,"Rates with Truth P_{T}","apl")
    #leg.AddEntry(h_Rate_Origin_WM,"Unfolding Method^{1}","apl")
    leg.AddEntry(h_Rate_Origin_WMRM,"Unfolding Method^{2}","apl")
    #leg.AddEntry(h_Rate_Origin_WMRRM,"Unfolding Method^{3}","apl")
    #leg.AddEntry(hR,"LH","apl")
    leg.Draw("same")
    pad2.cd()
    h_Rate_Origin0= h_Rate_Origin.Clone()
    h_Rate_Truth0 = h_Rate_Truth.Clone()
    h_Rate_Origin_WM0 = h_Rate_Origin_WM.Clone()
    h_Rate_Origin_WMRM0 = h_Rate_Origin_WMRM.Clone()
    h_Rate_Origin_WMRRM0 = h_Rate_Origin_WMRRM.Clone()
    h_Rate_Origin_WMRRM0.Divide(h_Rate_Origin0)
    h_Rate_Origin_WMRM0.Divide(h_Rate_Origin0)
    h_Rate_Origin_WM0.Divide(h_Rate_Origin0)
    h_Rate_Truth0.Divide(h_Rate_Origin0)
    hR0=hR.Clone()
    hR0.Divide(h_Rate_Origin0)
    h_Rate_Origin0.Divide(h_Rate_Origin0)
    h_Rate_Truth0.Draw("e")
    for Ceta2 in range(0,6):
          for Cet1 in range(0,4):
            label = str(ETBIN[Cet1])
            if Cet1 in [0]:
              label =label +", eta"+str(ETABIN[Ceta2])

            h_Rate_Truth0.GetXaxis().SetBinLabel(1+Cet1+4*Ceta2,label)
    
    
    pad2.SetBottomMargin(0.3) 
    h_Rate_Truth0.GetXaxis().SetLabelSize(0.09)
    h_Rate_Truth0.GetYaxis().SetLabelSize(0.09)
    h_Rate_Truth0.GetXaxis().SetTitle("Bin")
    h_Rate_Truth0.GetXaxis().SetTitleSize(0.1)
    #h_Rate_Truth0.SetLineWidth(0)
    #h_Rate_Truth0.SetMarkerSize(0)
    h_Rate_Origin0.Draw("esame")
    #h_Rate_Origin0.GetYaxis().SetRangeUser(min(h_Rate_Origin0.GetMinimum(),h_Rate_Truth0.GetMinimum()),max(h_Rate_Origin0.GetMaximum(),h_Rate_Truth0.GetMaximum()))
    h_Rate_Truth0.Draw("esame")
    #h_Rate_Origin_WM0.Draw("esame")
    h_Rate_Origin_WMRM0.Draw("esame")
    #h_Rate_Origin_WMRRM0.Draw("esame")
    #hR0.Draw("esame")
    #h_Rate_Truth0.Draw("esame")
    Can1.Print("M2.pdf")
         
   
   
   
   
   
   
   
   
   
   
    return
main()

