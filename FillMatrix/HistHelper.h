#include <TH2.h>
#include <vector>
#include <stdlib.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <algorithm>
#include <utility>
#include <fstream>
#include <sstream>
#include <cmath>
#include <TROOT.h>
#include <TFile.h>
#include <TLorentzVector.h>
#include <string>
#include <stdlib.h>


int ieta1,ieta2;
int iet1,iet2;

const double eta[7]={0.0, 0.6, 1.1, 1.52, 1.7, 2.3, 2.5};
   /*const double eta[51]={ 0.00
          , 0.02, 0.04, 0.06, 0.08 ,0.10
          , 0.20, 0.30, 0.40, 0.50, 0.60
          , 0.64, 0.68, 0.72, 0.76, 0.80
          , 0.87, 0.94, 1.01, 1.08, 1.15
          , 1.19, 1.23, 1.27, 1.31, 1.37
          , 1.40, 1.43, 1.46, 1.49, 1.52
          , 1.57, 1.62, 1.68, 1.74, 1.81
          , 1.85, 1.89, 1.93, 1.97, 2.01
          , 2.08, 2.15, 2.22, 2.29, 2.37
          , 2.39, 2.41, 2.43, 2.45, 2.47};
*/
   const double et[10]={0.0,20.0,30.0,40.0,50.0,60.0,70.0, 95.0, 130.0, 1000.0};

const int etavectLength = 6;//50;
const int etvectLength = 9;

std::vector<std::vector<TH1F*>> hv_el_Matrix=std::vector<std::vector<TH1F*>>(etvectLength,std::vector<TH1F*>(etavectLength));
std::vector<std::vector<TH1F*>> hv_el_MatrixR=std::vector<std::vector<TH1F*>>(etvectLength,std::vector<TH1F*>(etavectLength));
std::vector<std::vector<TH1F*>> hv_el_MatrixUW=std::vector<std::vector<TH1F*>>(etvectLength,std::vector<TH1F*>(etavectLength));
std::vector<std::vector<TH1F*>> hv_el_MatrixUWR=std::vector<std::vector<TH1F*>>(etvectLength,std::vector<TH1F*>(etavectLength));

std::vector<std::vector<TH1F*>> hv_el_RMatrix=std::vector<std::vector<TH1F*>>(etvectLength,std::vector<TH1F*>(etavectLength));
std::vector<std::vector<TH1F*>> hv_el_RMatrixR=std::vector<std::vector<TH1F*>>(etvectLength,std::vector<TH1F*>(etavectLength));
std::vector<std::vector<TH1F*>> hv_el_RMatrixUW=std::vector<std::vector<TH1F*>>(etvectLength,std::vector<TH1F*>(etavectLength));
std::vector<std::vector<TH1F*>> hv_el_RMatrixUWR=std::vector<std::vector<TH1F*>>(etvectLength,std::vector<TH1F*>(etavectLength));


TH2F* h_el_ptR_Wrong;
TH2F* h_el_ptR_Right;
TH2F* h_el_ptR_WrongUW;
TH2F* h_el_ptR_RightUW;

TH2F* h_el_ptT_Wrong;
TH2F* h_el_ptT_Right;
TH2F* h_el_ptT_WrongUW;
TH2F* h_el_ptT_RightUW;

TH2F* h_el_Counting_W;
TH2F* h_el_Counting_R;
TH2F* h_el_Counting_WT;
TH2F* h_el_Counting_RT;


TH1F* h_el_pt_WrongCharged = new TH1F("h_el_pt_WrongCharged","h_el_pt_WrongCharged",1000,0,1000.);
TH1F* h_el_pt_RightCharged = new TH1F("h_el_pt_RightCharged","h_el_pt_RightCharged",1000,0,1000.);
TH1F* h_el_Et_WrongCharged = new TH1F("h_el_Et_WrongCharged","h_el_Et_WrongCharged",1000,0,1000.);
TH1F* h_el_Et_RightCharged = new TH1F("h_el_Et_RightCharged","h_el_Et_RightCharged",1000,0,1000.);

TH1F* h_el_mass_OS = new TH1F("h_el_mass_OS","h_el_mass_OS",200,50,150.);
TH1F* h_el_mass_SS = new TH1F("h_el_mass_SS","h_el_mass_SS",200,50,150.);
TH1F* h_el_massT_OS = new TH1F("h_el_massT_OS","h_el_massT_OS",200,50,150.);
TH1F* h_el_massT_SS = new TH1F("h_el_massT_SS","h_el_massT_SS",200,50,150.);


TH1F* h_el_pt_WrongCharged_Truth = new TH1F("h_el_pt_WrongCharged_Truth","h_el_pt_WrongCharged_Truth",1000,0,1000.);
TH1F* h_el_pt_RightCharged_Truth = new TH1F("h_el_pt_RightCharged_Truth","h_el_pt_RightCharged_Truth",1000,0,1000.);




TH2F* h_el_PT_response_wrong = new TH2F("h_el_PT_response_wrong","h_el_PT_response_wrong;pt_reco;pt_truth",150,0,150.,150,0,150.);
TH2F* h_el_PT_response_right = new TH2F("h_el_PT_response_right","h_el_PT_response_right;pt_reco;pt_truth",150,0,150.,150,0,150.);


void BuildHistGroup(std::string WP){
char name[256];
char name100[256];
sprintf(name,"h_el_%s_ptR_Wrong",WP.c_str());
h_el_ptR_Wrong= new TH2F(name,name,320, 20 ,180,6,eta );
sprintf(name,"h_el_%s_ptR_Right",WP.c_str());
h_el_ptR_Right= new TH2F(name,name,320, 20 ,180,6,eta );
sprintf(name,"h_el_%s_ptR_WrongUW",WP.c_str());
h_el_ptR_WrongUW= new TH2F(name,name,320, 20 ,180,6,eta );
sprintf(name,"h_el_%s_ptR_RightUW",WP.c_str());
h_el_ptR_RightUW= new TH2F(name,name,320, 20 ,180,6,eta );

sprintf(name,"h_el_%s_ptT_Wrong",WP.c_str());
h_el_ptT_Wrong= new TH2F(name,name,320, 20 ,180,6,eta );
sprintf(name,"h_el_%s_ptT_Right",WP.c_str());
h_el_ptT_Right= new TH2F(name,name,320, 20 ,180,6,eta );
sprintf(name,"h_el_%s_ptT_WrongUW",WP.c_str());
h_el_ptT_WrongUW= new TH2F(name,name,320, 20 ,180,6,eta );
sprintf(name,"h_el_%s_ptT_RightUW",WP.c_str());
h_el_ptT_RightUW= new TH2F(name,name,320, 20 ,180,6,eta );

sprintf(name,"Counting_%s_W",WP.c_str());
h_el_Counting_W= new TH2F(name,name,10, -0.5 ,9.5,   50,-0.5,49.5 );
sprintf(name,"Counting_%s_R",WP.c_str());
h_el_Counting_R= new TH2F(name,name,10, -0.5 ,9.5,   50,-0.5,49.5 );
sprintf(name,"Counting_%s_WT",WP.c_str());
h_el_Counting_WT= new TH2F(name,name,10, -0.5 ,9.5,50,-0.5,49.5 );
sprintf(name,"Counting_%s_RT",WP.c_str());
h_el_Counting_RT= new TH2F(name,name,10, -0.5 ,9.5,50,-0.5,49.5 );


for(iet1 = 0 ; iet1 < etvectLength; iet1 ++){
for(ieta2 = 0 ; ieta2 < etavectLength ; ieta2 ++){

sprintf(name,"Matrix_et%deta%3.2f",iet1,eta[ieta2]);
 sprintf(name100,"%s_Matrix_Reco/Matrix_et%deta%3.2f",WP.c_str(),iet1,eta[ieta2]);
 hv_el_Matrix[iet1][ieta2] = new TH1F(name,name100, etvectLength, et);
// sprintf(name,"MatrixR_et%deta%3.2f",iet1,eta[ieta2]);

sprintf(name,"MatrixR_et%deta%3.2f",iet1,eta[ieta2]);
sprintf(name100,"%s_MatrixR_Reco/MatrixR_et%deta%3.2f",WP.c_str(),iet1,eta[ieta2]);
 hv_el_MatrixR[iet1][ieta2] = new TH1F(name,name100, etvectLength,et);

 sprintf(name,"MatrixUW_et%deta%3.2f",iet1,eta[ieta2]);
sprintf(name100,"%s_MatrixUW_Reco/MatrixUW_et%deta%3.2f",WP.c_str(),iet1,eta[ieta2]);
 hv_el_MatrixUW[iet1][ieta2] = new TH1F(name,name100, etvectLength,et);

sprintf(name,"MatrixUWR_et%deta%3.2f",iet1,eta[ieta2]);
sprintf(name100,"%s_MatrixUWR_Reco/MatrixUWR_et%deta%3.2f",WP.c_str(),iet1,eta[ieta2]);
 hv_el_MatrixUWR[iet1][ieta2] = new TH1F(name,name100, etvectLength,et);

 sprintf(name,"RMatrix_et%deta%3.2f",iet1,eta[ieta2]); 
  sprintf(name100,"%s_RMatrix_Reco/RMatrix_et%deta%3.2f",WP.c_str(),iet1,eta[ieta2]);
 hv_el_RMatrix[iet1][ieta2] = new TH1F(name,name100, etvectLength, et);


sprintf(name,"RMatrixR_et%deta%3.2f",iet1,eta[ieta2]);
sprintf(name100,"%s_RMatrixR_Reco/RMatrixR_et%deta%3.2f",WP.c_str(),iet1,eta[ieta2]);
 hv_el_RMatrixR[iet1][ieta2] = new TH1F(name,name100, etvectLength,et);

 sprintf(name,"RMatrixUW_et%deta%3.2f",iet1,eta[ieta2]);
 sprintf(name100,"%s_RMatrixUW_Reco/RMatrixUW_et%deta%3.2f",WP.c_str(),iet1,eta[ieta2]);
 hv_el_RMatrixUW[iet1][ieta2] = new TH1F(name,name100, etvectLength,et);

sprintf(name,"RMatrixUWR_et%deta%3.2f",iet1,eta[ieta2]);
sprintf(name100,"%s_RMatrixUWR_Reco/RMatrixUWR_et%deta%3.2f",WP.c_str(),iet1,eta[ieta2]);
 hv_el_RMatrixUWR[iet1][ieta2] = new TH1F(name,name100, etvectLength,et);

}
}
//std::cout<<"?"<<std::endl;
}
