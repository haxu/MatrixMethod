#include <TChain.h>
#include "HistHelper.h"
Int_t           isTagTag;
Int_t           elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest;
Int_t           elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest;
Float_t         elCand1_eta;
Float_t         elCand1_pt;
Float_t         elCand1_phi;
Float_t         elCand1_charge;
Float_t         elCand1_d0significance;
Int_t           elCand1_FirstEgMotherTyp;
Int_t           elCand1_FirstEgMotherOrigin;
Float_t         elCand1_firstEgMother_Pt;
Float_t         elCand1_firstEgMother_Eta;
Float_t         elCand1_firstEgMother_Phi;
Int_t           elCand1_type;
Int_t           elCand1_origin;
Int_t           elCand2_origin;
Int_t           elCand1_firstEgMotherPdgId;
 Int_t           isMultiElectronTriggerMatched;
Int_t           elCand1_isTriggerMatched;
Int_t           elCand2_isTriggerMatched;
Float_t         MCPileupWeight;
Float_t         MCEventWeight;
Float_t         Zcand_M;
Int_t           elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest;
Int_t           elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest;
Int_t           elCand1_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest;
Int_t           elCand2_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest;

Int_t           elCand1_LooseLLH_DataDriven_d0z0_Rel21_Smooth_vTest;
Int_t           elCand2_LooseLLH_DataDriven_d0z0_Rel21_Smooth_vTest;


Float_t         elCand2_eta;
Float_t         elCand2_pt;
Float_t         elCand2_phi;
Float_t         elCand2_charge;
Float_t         elCand2_d0significance;
Int_t           elCand2_FirstEgMotherTyp;
Int_t           elCand2_FirstEgMotherOrigin;
Float_t         elCand2_firstEgMother_Pt;
Float_t         elCand2_firstEgMother_Eta;
Float_t         elCand2_firstEgMother_Phi;
Float_t elCand1_et;
Float_t elCand2_et;
Int_t           elCand2_type;
Int_t           elCand2_firstEgMotherPdgId;
Int_t Nvtx;
Int_t           elCand1_isolFixedCutTightTrackOnly;
Int_t           elCand2_isolGradient;
Int_t           elCand1_isolGradient;
Int_t           elCand2_isolFixedCutTightTrackOnly;

Int_t elCand2_isolGradientLoose;
Int_t elCand1_isolGradientLoose;
Int_t elCand2_FixedCutTight;
Int_t elCand1_FixedCutTight;


Float_t         newMass; 
Float_t         newMass2;
Float_t         newMass3;
Float_t elCand2_CFTmedium;
Float_t elCand1_CFTmedium;
void SetAVarAddress(TTree *fChain){
fChain->SetBranchAddress("elCand1_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest", &elCand1_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest);
fChain->SetBranchAddress("elCand2_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest", &elCand2_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest);

fChain->SetBranchAddress("elCand2_isolGradientLoose", &elCand2_isolGradientLoose);
fChain->SetBranchAddress("elCand1_isolGradientLoose", &elCand1_isolGradientLoose);

fChain->SetBranchAddress("elCand2_FixedCutTight", &elCand2_FixedCutTight);
fChain->SetBranchAddress("elCand1_FixedCutTight", &elCand1_FixedCutTight);

fChain->SetBranchAddress("elCand1_et", &elCand1_et);
fChain->SetBranchAddress("elCand2_et", &elCand2_et);

fChain->SetBranchAddress("elCand2_CFTmedium", &elCand2_CFTmedium);
fChain->SetBranchAddress("elCand1_CFTmedium", &elCand1_CFTmedium);
fChain->SetBranchAddress("elCand2_isolGradient", &elCand2_isolGradient);
fChain->SetBranchAddress("elCand1_isolGradient", &elCand1_isolGradient);
fChain->SetBranchAddress("elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest", &elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest);
fChain->SetBranchAddress("elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest", &elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest);
fChain->SetBranchAddress("elCand1_isolFixedCutTightTrackOnly", &elCand1_isolFixedCutTightTrackOnly);
fChain->SetBranchAddress("elCand2_isolFixedCutTightTrackOnly", &elCand2_isolFixedCutTightTrackOnly);
fChain->SetBranchAddress("Nvtx", &Nvtx);
fChain->SetBranchAddress("isMultiElectronTriggerMatched", &isMultiElectronTriggerMatched);
fChain->SetBranchAddress("elCand1_isTriggerMatched", &elCand1_isTriggerMatched);
fChain->SetBranchAddress("elCand1_isTriggerMatched", &elCand2_isTriggerMatched);
fChain->SetBranchAddress("isTagTag", &isTagTag);
fChain->SetBranchAddress("elCand1_type", &elCand1_type);
fChain->SetBranchAddress("elCand2_type", &elCand2_type);
fChain->SetBranchAddress("elCand1_origin", &elCand1_origin);
fChain->SetBranchAddress("elCand2_origin", &elCand2_origin);
fChain->SetBranchAddress("elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest", &elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest);
fChain->SetBranchAddress("elCand1_eta", &elCand1_eta);
fChain->SetBranchAddress("elCand1_pt", &elCand1_pt);
fChain->SetBranchAddress("elCand1_phi", &elCand1_phi);
fChain->SetBranchAddress("elCand1_charge", &elCand1_charge);
fChain->SetBranchAddress("elCand1_d0significance", &elCand1_d0significance);
fChain->SetBranchAddress("elCand1_FirstEgMotherTyp", &elCand1_FirstEgMotherTyp);
fChain->SetBranchAddress("elCand1_FirstEgMotherOrigin", &elCand1_FirstEgMotherOrigin);
fChain->SetBranchAddress("elCand1_firstEgMother_Pt", &elCand1_firstEgMother_Pt);
fChain->SetBranchAddress("elCand1_firstEgMotherPdgId", &elCand1_firstEgMotherPdgId);
fChain->SetBranchAddress("elCand1_firstEgMother_Eta", &elCand1_firstEgMother_Eta);
fChain->SetBranchAddress("elCand1_firstEgMother_Phi", &elCand1_firstEgMother_Phi);
fChain->SetBranchAddress("MCPileupWeight", &MCPileupWeight);
fChain->SetBranchAddress("Zcand_M", &Zcand_M);
fChain->SetBranchAddress("elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest", &elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest);
fChain->SetBranchAddress("elCand2_eta", &elCand2_eta);
fChain->SetBranchAddress("elCand2_pt", &elCand2_pt);
fChain->SetBranchAddress("elCand2_phi", &elCand2_phi);
fChain->SetBranchAddress("elCand2_charge", &elCand2_charge);
fChain->SetBranchAddress("elCand2_d0significance", &elCand2_d0significance);
fChain->SetBranchAddress("elCand2_FirstEgMotherTyp", &elCand2_FirstEgMotherTyp);
fChain->SetBranchAddress("elCand2_FirstEgMotherOrigin", &elCand2_FirstEgMotherOrigin);
fChain->SetBranchAddress("elCand2_firstEgMother_Pt", &elCand2_firstEgMother_Pt);
fChain->SetBranchAddress("elCand2_firstEgMotherPdgId", &elCand2_firstEgMotherPdgId);
fChain->SetBranchAddress("elCand2_firstEgMother_Eta", &elCand2_firstEgMother_Eta);
fChain->SetBranchAddress("elCand2_firstEgMother_Phi", &elCand2_firstEgMother_Phi);
}


bool ApplyPreCuts(){
if(!(isTagTag==0)) return false;;
if(!(isMultiElectronTriggerMatched)){return false;;}
//if((elCand1_d0significance>5.)||(elCand2_d0significance>5.)){return false;;}
if((elCand1_pt<25000.)||(elCand2_pt<25000.)){return false;;}
if(((elCand1_eta>1.37)&&(elCand1_eta<1.52))||((elCand2_eta>1.37)&&(elCand2_eta<1.52))){return false;;}
if(!((elCand1_type==2)||((elCand1_origin==5) && (fabs(elCand1_firstEgMotherPdgId)==11)))){return false;;}
if(!((elCand2_type==2)||((elCand2_origin==5) && (fabs(elCand1_firstEgMotherPdgId)==11)))){return false;;}
if((Zcand_M<70000.)||(Zcand_M>110000.)){return false;;}
return true;
}
bool Lep_1_bin(int iet,int ieta){
	if((fabs(elCand1_pt)>et[iet]*1000.)&&(fabs(elCand1_pt)<et[iet+1]*1000.)&&(fabs(elCand1_eta)>eta[ieta])&&(fabs(elCand1_eta)<eta[ieta+1])) {return true;}
	else{return false;}
}
bool Lep_2_bin(int iet,int ieta){
if((fabs(elCand2_pt)>et[iet]*1000.)&&(fabs(elCand2_pt)<et[iet+1]*1000.)&&(fabs(elCand2_eta)>eta[ieta])&&(fabs(elCand2_eta)<eta[ieta+1])) {return true;}
	else{return false;}
}


bool Lep_1_Truthbin(int iet,int ieta){
	if((fabs(elCand1_firstEgMother_Pt)>et[iet]*1000.)&&(fabs(elCand1_firstEgMother_Pt)<et[iet+1]*1000.)&&(fabs(elCand1_eta)>eta[ieta])&&(fabs(elCand1_eta)<eta[ieta+1])) {return true;}
	else{return false;}
}
bool Lep_2_Truthbin(int iet,int ieta){
if((fabs(elCand2_firstEgMother_Pt)>et[iet]*1000.)&&(fabs(elCand2_firstEgMother_Pt)<et[iet+1]*1000.)&&(fabs(elCand2_eta)>eta[ieta])&&(fabs(elCand2_eta)<eta[ieta+1])) {return true;}
	else{return false;}
}


bool WPCuts(std::string WP){
if(WP=="TightTT"){
if(!((elCand1_isolFixedCutTightTrackOnly==1)&&(elCand2_isolFixedCutTightTrackOnly==1))) {return false;}
if(!(elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}

if(WP=="TightT"){
if(!((elCand1_FixedCutTight==1)&&(elCand2_FixedCutTight==1))) {return false;}
if(!(elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}

if(WP=="TightGL"){
if(!((elCand2_isolGradientLoose==1)&&(elCand2_isolGradientLoose==1))) {return false;}
if(!(elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}

if(WP=="TightG"){
if(!((elCand2_isolGradient==1)&&(elCand2_isolGradient==1))) {return false;}
if(!(elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}

if(WP=="MediumTT"){
if(!((elCand1_isolFixedCutTightTrackOnly==1)&&(elCand2_isolFixedCutTightTrackOnly==1))) {return false;}
if(!(elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}

if(WP=="MediumT"){
if(!((elCand1_FixedCutTight==1)&&(elCand2_FixedCutTight==1))) {return false;}
if(!(elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}


if(WP=="MediumG"){
if(!((elCand1_isolGradient==1)&&(elCand2_isolGradient==1))) {return false;}
if(!(elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}
if(WP=="MediumGL"){
if(!((elCand1_isolGradientLoose==1)&&(elCand2_isolGradientLoose==1))) {return false;}
if(!(elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}
	

if(WP=="Medium"){
if(!(elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}

if(WP=="Tight"){
if(!(elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}

if(WP=="LooseBL"){
if(!(elCand1_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand2_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
return true;
}



if(WP=="MediumTCFT"){
if(!((elCand1_isolFixedCutTightTrackOnly==1)&&(elCand2_isolFixedCutTightTrackOnly==1))) {return false;}
if(!(elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest>0)) {return false;}
if(!(elCand2_CFTmedium>0.106639)){return false;}
if(!(elCand1_CFTmedium>0.106639)){return false;}
return true;
}

return false;
}

