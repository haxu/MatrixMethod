#!/bin/bash
#$ -cwd
#$ -j y
#$ -t 1-10:1
#$ -l os=sl6
#$ -l ct=20:00:00
#$ -l vmem=4G
#$ -l fsize=4096M
#$ -l sps=1
File_List=mc.list
NFilesPerJob=10
if [ "$SGE_TASK_ID" == "" ]; then
        SGE_TASK_ID=1
fi
index=${SGE_TASK_ID}
index0=$((${SGE_TASK_ID}-1))
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup root
total_length=$(wc -l ${File_List} |awk '{print $1}')
for(( i = 0 ; i < ${NFilesPerJob} ; i++ )); do
        index1=$(( ${index0}*${NFilesPerJob}+${i}+1 ))
        if (( $index1 > $total_length )); then
                echo "out of range"
        else
                inputfile=$(head -n ${index1} ${File_List} |tail -n 1)
                tag1=${inputfile%.*}
                ./EB $tag1
                echo $tag1  "FIND"
        fi
done
mv runFull.sh.o* /sps/atlas/h/hanlin/QMISID/batchLog/ 
